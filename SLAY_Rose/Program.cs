﻿using System;
using System.Linq;

namespace SLAY_Rose
{
    delegate double SystemOfFunction(double[] roots, int NumberEquation);

    class MethodOfNewton
    {
        int CountIteration;//количество итераций
        double GoldX, x0;//объявляються х
        double a, b;//диапазон
        double epsilon;//точность

        double Function(double x)
        {
            return Math.Pow(x, 2) * 2 + Math.Pow(x, 3) * 7 - 9;//функция
        }

        double Derivative(double x)
        {
            return 4 * x + 21 * Math.Pow(x, 2);
        }//производная

        double SecondDerivative(double x)
        {
            return 4 + 42 * x;
        }//вторая производная

        public void GetRange()//устоновить границы
        {
            double A, B;
            bool badRange = true;
            do
            {
                Console.WriteLine("Введите левую границу!");
                while (!Double.TryParse(Console.ReadLine(), out A))
                {
                    Console.WriteLine("Попробуйте снова ввести действительное число");
                }
                Console.WriteLine("Введите правую границу!");
                while (!Double.TryParse(Console.ReadLine(), out B))
                {
                    Console.WriteLine("Попробуйте снова ввести действительное число");
                }
                if (A > B)
                {
                    Console.WriteLine("Вы перепутали границы!");
                }
                if (Function(A) * Function(B) > 0)
                {
                    badRange = false;
                    Console.WriteLine("В этом диапозоне корня нет!");
                }

            } while ((A > B) && badRange);

            a = A; b = B;

        }

        public void GetEpsilon()
        {
            double e;
            int count = 0;
            do
            {
                do
                {
                    Console.WriteLine("Введите эпсилон!");
                    count++;
                    if (count > 5)
                    {
                        Console.WriteLine("Это действительное число если что! А не символ или что то такое *~* ");
                        count = 0;
                    }
                } while (!double.TryParse(Console.ReadLine(), out e));
                if (e < 0)
                    Console.WriteLine("Эпсилон должен быть больше 0");
            } while (e < 0);
            epsilon = e;
        }//установить точность

        public void DoIterations()//главный цикл метода
        {
            if (Function(a) * SecondDerivative(a) > 0)//свойство исли функцию * вторую производную (в левой границе) и произведение больше нуля то начинаем искать x с левой границы
                x0 = a;
            else
                x0 = b;//иначе с правой
            Console.WriteLine("Итерация{0}: x0={1}", CountIteration, x0);

            GoldX = x0 - Function(x0) / Derivative(x0);//goldx это конечный ответ(равен предыдущий x - функция на производную)
            Console.WriteLine("Итерация{0}: xn={1}", CountIteration, GoldX);
            CountIteration++;//вывели итерацию 

            while (Math.Abs(x0 - GoldX) > epsilon)//в цыкле пока разность x нового и старого не станет меньше точности
            {
                x0 = GoldX;
                GoldX = x0 - Function(x0) / Derivative(x0);//goldx это конечный ответ(равен предыдущий x - функция разделеная на производную)
                Console.WriteLine("Итерация{0}: xn={1}  |F(x)|={2}   |xn - x(n-1)| = {3}", CountIteration, GoldX, Math.Abs(Function(GoldX)), Math.Abs(x0 - GoldX));
                CountIteration++;
            }

            Console.WriteLine("\nОтвет: {0} ", GoldX);
            Console.WriteLine("Проверка: Значение функции в точке x = {0} !", Function(GoldX));
            Console.ReadLine();
        }//ура конец


    }

    class MethodOfGauss//метод гаусса
    {
        int n, m;
        double[] roots;
        double[,] coefficients_SLAE;
        public bool CanShowInfo = false;

        public double[] Roots
        {
            get{ return roots;}
        }

        public void SetMatrixSize(int N,int M)
        {
            if (N <= M - 1 && N>0)
            {
                n = N;m = M;
            }
        }

        public double[,] Coefficients_SLAE
        {
            set { Coefficients_SLAE = value; }
        }

        public MethodOfGauss(int N, int M, double[,] Coefficients_SLAE)
        {
            n = N;m = M; coefficients_SLAE = Coefficients_SLAE;roots = new double[n];
        }

        public double [] Solve()
        {
            if (m * n == coefficients_SLAE.Length)//если не критичное условие
            {
                GetTriangularMatrix();//приводим матрицу к треугольному виду
                FindRoots_SLAE();//корни
            }
            return roots;
        }

        void GetTriangularMatrix()
        {
            for (int k = 0; k < n-1; k++)//бежим по столбцам сверху вниз
            {
                if(CanShowInfo)
                    ShowMatrix();//аоказать матрицу

                int maxPos = GetMaxPos(k);//находим максимальный элемент начиная с указаной строчки
                SwapRow(maxPos,k);//обменяем с текущей строчку с максимальным элементом
                if (CanShowInfo)
                {
                    Console.WriteLine("Поменяли строки {0} и {1} между собой", maxPos, k);
                    ShowMatrix();//отображаем матрицу
                }

                NormalizeEquations(k);//нормализуем строчку(самый максимальный 1 остальные они сами деленые на максимальный)
                if (CanShowInfo)
                {
                    Console.WriteLine("Получили 1 в {0} столбце", k);
                    ShowMatrix();
                }

                Subtract(k);
                if (CanShowInfo)
                {
                    Console.WriteLine("Получили 0 в {0} столбце", k);
                    ShowMatrix();
                }
            }

            NormalizeEquations(n-1);
            if(CanShowInfo)
                ShowMatrix();
        }

        public void ShowMatrix()
        {
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                    Console.Write(coefficients_SLAE[i, j] + " ");
                Console.WriteLine();
            }
            Console.WriteLine();

        }

        int GetMaxPos(int K)
        {
            int IndexMax=0;
            for (int i = K; i < n; i++)
            {
                if (Math.Abs(coefficients_SLAE[IndexMax,K])<Math.Abs(coefficients_SLAE[i,K]))
                {
                    IndexMax = i;
                }
            }
            return IndexMax;
        }

        void SwapRow (int RowOne, int RowTwo)
        {
            double temp;
            for(int i = 0; i < m; i++)
            {
                temp = coefficients_SLAE[RowOne, i];
                coefficients_SLAE[RowOne, i] = coefficients_SLAE[RowTwo, i];
                coefficients_SLAE[RowTwo, i] = temp;
            }
        }

        void NormalizeEquations(int K)
        {
            for (int i = K; i < n; i++)
            {
                double del = coefficients_SLAE[i, K];
                for (int j = K; j < m; j++)
                {
                    coefficients_SLAE[i, j] /= del;
                }
            }
        }

        void Subtract(int K)
        {
            for(int i =K+1;i<n;i++)
                for(int j = 0; j < m; j++)
                {
                    coefficients_SLAE[i, j] -= coefficients_SLAE[K, j];
                }
        }

        void FindRoots_SLAE()
        {
            double[] x = roots;

            for(int i = n-1; i >= 0 ; i--)
            {
                x[i] = coefficients_SLAE[i, m - 1];
                for (int j = i+1; j < n;j ++)
                     x[i] -= coefficients_SLAE[i, j] * x[j];
            }
        }
    }

    class SneMethodOfNewton
    {
        double[] roots;
        int n;
        double[,] m_Jacoby;
        SystemOfFunction SF;
        double epsilon;
        public bool isCanShowInfo = false;

        public SneMethodOfNewton(double[] Roots, SystemOfFunction sf, double Epsilon)
        {
            n = Roots.Length; roots = Roots; SF = sf; epsilon = Epsilon; m_Jacoby = new double[n, n+1];
        }

        public double[] Solve()
        {
            bool isAll = false;

            if (isCanShowInfo)
            {
                Console.WriteLine("Первое приблежение: ");
                ShowRoots();
            }

            int countIteration=0;
            do
            {
                if(isCanShowInfo)
                    Console.WriteLine("Итерация {0}:",countIteration);

                Find_m_Jacoby();//находим якоби
                MethodOfGauss _slae = new MethodOfGauss(n, n + 1, m_Jacoby);//решаем матрицу якоби методом гаусса
                double[] rootsSLAE = _slae.Solve();//решили получаем корни
                FindNewRoots(rootsSLAE);//находим новые корни

                if (isCanShowInfo)
                {
                    Console.WriteLine("Новые корни:");
                    ShowRoots();
                }

                if (GetMaxAbs(rootsSLAE) < epsilon)//проверяем точность
                    isAll = true;
                if (isCanShowInfo)
                {
                    Console.WriteLine("deltaX = {0}\n", GetMaxAbs(rootsSLAE));
                }

                countIteration++;
            } while (!isAll);

            return roots;
        }

        void Find_m_Jacoby()//находим матрицу якоби
        {
            double h = 0.00000001;
            for(int i=0;i<n;i++)
                for(int j =0; j < n; j++)
                {
                    roots[j] += h;
                    double F1 = SF(roots, i);//функция слева
                    roots[j] -= 2* h;
                    double F2 = SF(roots, i);//функция справа
                    roots[j] += h;
                    m_Jacoby[i, j] = (F1-F2) / (2 * h);
                }
            for(int i = 0; i < n; i++)
            {
                m_Jacoby[i, n] = -SF(roots, i);
            }
        }

        void FindNewRoots(double[] RootsSLAE)
        {
            for (int i = 0; i < n; i++)
                roots[i] += RootsSLAE[i];//вычитаем из приближений корни которые получили в матрице якоби
        }

        double GetMaxValueFunc(double [] RootsSLAE)
        {
            double max = 0;
            double[] deltaRoots = new double[n];
            for(int i = 0;i<n;i++)
                deltaRoots[i] = roots[i] - RootsSLAE[i];
            for (int i = 0; i < n; i++)
            {
                if (Math.Abs(SF(roots, i) - SF(deltaRoots, i)) > max)
                    max = Math.Abs(SF(roots, i) - SF(deltaRoots, i));
            }
            return max;
        }

        void ShowRoots()
        {
            for (int i = 0; i < roots.Length; i++)
                Console.Write("x[{0}] = {1}; ", i, roots[i]);
            Console.WriteLine("\n");
        }

        double GetMaxAbs(double[] Array)
        {
            double max = 0;
            for(int i = 0; i < Array.Length; i++)
            {
                if (max < Math.Abs(Array[i]))
                    max = Math.Abs(Array[i]);
            }
            return max;
        }
    }//решение системы не линейных уравнений методом ньютона

    class Program
    {
        static void Main(string[] args)
        {
            //Метод ньютона

           /* MethodOfNewton newton = new MethodOfNewton();//метод ньютона нужно раскоментировать и запустить
            newton.GetRange();//спрашиваеться диапозон
            newton.GetEpsilon();/спрашивается точность
            newton.DoIterations();*/ //выполняеться код


            //GaussWithMenu();

            TestSNENewton();

            Console.Read();
        }

        static void TestGauss()
        {
            double[,] test = { { 0.12, 0.18, -0.17, 5.5 }, { 0.06, 0.09, 0.15, -1.95 }, { 0.22, -0.1, 0.06, 0.5 } }; //{ {2,4, 1,36}, { 5,2,1,47},{2,3,4,37}};
            MethodOfGauss _slae = new MethodOfGauss(3, 4, test);
            _slae.Solve();
            Console.WriteLine("Корни");
            foreach(double x in _slae.Roots)
            {
                Console.Write(x + " ");
            }
        }

        static void GaussWithMenu()
        {
            Console.WriteLine("Введите количество уравнений:");
            int n = GetN();
            int m = n + 1;
            Console.WriteLine("Введите матрицу!");
            double[,] coeffi = GetMatrix(n, m);
            Console.WriteLine("Cпасибо. Сейчас решим систему...");
            MethodOfGauss _slae = new MethodOfGauss(n, m, coeffi);
            double[] roots = _slae.Solve();
            Console.WriteLine("Ответ");
            foreach(double x in roots)
            {
                Console.Write(x + " ");
            }
        }

        static int GetN()
        {
            bool goodM = false;
            int m;

            do
            {
                if (!Int32.TryParse(Console.ReadLine(), out m))
                    Console.WriteLine("Нет. Вы должны ввести число!");
                else if (m < 1)
                    Console.WriteLine("Введите положительное число не равное нолю");
                else
                    goodM = true;
            } while (!goodM);

                return m;
        }

        static double [,] GetMatrix(int N,int M)
        {
            double[,] matrix = new double[N, M];
            for(int i=0;i<N;i++)
                for( int j = 0;j < M;j++)
                {
                    Console.Write("a[{0},{1}]=", i, j);
                    matrix[i, j] = Double.Parse(Console.ReadLine());
                }
            return matrix;
        }

        static void TestSNENewton()
        {
            Console.WriteLine("Сколько уравнений? 3");
            Console.WriteLine("Введите эпсилон 0.0000001");
            Console.WriteLine("Решаем систему");

            double[] roots0 = { 0.05, 0.9, 2.4 };//начальные горни
            SneMethodOfNewton _sne = new SneMethodOfNewton(roots0, TestSF, 0.000000000001);//создаем класс который почитает нам все(передаем ему корни, тестовую функцию и точность)
            _sne.isCanShowInfo = true;//показывать шаги по этерациям
            roots0 = _sne.Solve();//решаем систему

            for(int i=0; i < 3; i++)
            {
                Console.WriteLine("Корень{0}={1}", i, roots0[i]);
            }
            for(int i = 0; i < 3; i++)
            {
                Console.WriteLine("Уравнение {0} = {1}", i, TestSF(roots0, i));
            }
        }

        static double TestSF(double [] Roots, int NumEquations)
        {
            switch (NumEquations)//система уравнений
            {
                case 0: return (Math.Sqrt(Roots[0]) + Math.Sqrt(Roots[1]*Roots[1]+Roots[2]*Roots[2]) - 2.563448);
                case 1: return (Math.Cos(Roots[0] + Roots[1]) + Math.Atan(Roots[1] * Roots[2]) - 1.4249646);
                case 2: return (Math.Exp(-(Roots[0] * Roots[0] + Roots[1] * Roots[1]) / 2) + Math.Log(Roots[1] + Roots[2]) - 1.6157266);
            }
            return 0;
        }
    }
}
